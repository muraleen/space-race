package com.silverwing.spacerace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Lander {
	
	public Sprite lander, gear1, gear2, heat_shield;
	public Animation thr_fwd, thr_left, thr_right;
	public PropertyList props;
	public String dir;
	
	public Lander(String name) {
		props = new PropertyList();
		
		// Load lander data file
		dir = "spacecraft/"+name+"/lander/";
		FileHandle fileHandle = Gdx.files.internal(dir+"lander.dat");
		
		if(fileHandle.exists()) {
			
			props = SRDataInterface.readPropsFile(fileHandle);
			
			lander = GDXHelper.loadSprite(dir+props.getProp("main-texture"));
			gear1 = GDXHelper.loadSprite(dir+props.getProp("gear-texture"));
			gear2 = GDXHelper.loadSprite(dir+props.getProp("gear-texture"));
			heat_shield = GDXHelper.loadSprite(dir+props.getProp("heat-shield"));
			
			
			
			// Initialize thruster animations
			Texture thr_fwd_texture = GDXHelper.loadTexture(dir+props.getProp("thr-main"));
			thr_fwd = new Animation(0.03125f,TextureRegion.split(thr_fwd_texture, thr_fwd_texture.getWidth()/4, thr_fwd_texture.getHeight())[0]);
			Texture thr_left_texture = GDXHelper.loadTexture(dir+props.getProp("thr-left"));
			thr_left = new Animation(0.0625f,TextureRegion.split(thr_left_texture, thr_left_texture.getWidth()/4, thr_left_texture.getHeight())[0]);
			Texture thr_right_texture = GDXHelper.loadTexture(dir+props.getProp("thr-left"));
			thr_right = new Animation(0.0625f,TextureRegion.split(thr_right_texture, thr_right_texture.getWidth()/4, thr_right_texture.getHeight())[0]);
			
		}
		
	}

}
