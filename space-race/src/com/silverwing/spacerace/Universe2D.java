// SpaceRace: 2D Universe Planetary Physics Engine
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Universe2D {
	
	public static float JYEAR2SEC = 31557600; // 1 Julian year (365.25 earth days) in seconds

	public ArrayList<Star> stars = new ArrayList<Star>();
	// public ArrayList<BackgroundStar> bgStars = new ArrayList<BackgroundStar>();
	public Sprite bg1Sprite, bg2Sprite, bg3Sprite, orbit, bodyOrbit;
	public float G = (float) 6.67384E-11;
	public CelestialBody orbitalCenter = new CelestialBody();
	public CelestialBody obcStar = new CelestialBody();
	public CelestialBody obcPlanet = new CelestialBody();
	public CelestialBody obcMoon = new CelestialBody();
	public boolean drawOrbit = false;
	CelestialBody returnBody;
	float plRad_scr, radius, th_factor, e_min, e_minPl, e_minMn, e_minSt, mu, e, mu_pl, e_pl, mu_mn, e_mn, probFactor, theta, phi, r, v, posHdg, beta, p, bugfix;
	Vector bgTimer, location, relPos, relVel, orbPos, plCtr_scr, scr_offset;
	int last_x, last_y, layer;
	
	public Universe2D(ArrayList<Star> stars, Vector screen) {
		this.stars = stars;
		// starSprite = new Sprite(new Texture(Gdx.files.internal(textureLoc)));
		Texture bg1Texture = new Texture(Gdx.files.internal("background/stars_1.png"));
		Texture bg2Texture = new Texture(Gdx.files.internal("background/stars_2.png"));
		Texture bg3Texture = new Texture(Gdx.files.internal("background/stars_3.png"));
		
		bg1Texture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		bg2Texture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		bg3Texture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
		
		bg1Sprite = new Sprite(bg1Texture); //,0,0,1024,1024);
		bg2Sprite = new Sprite(bg2Texture); //,0,0,1024,1024);
		bg3Sprite = new Sprite(bg3Texture); //,0,0,1024,1024);
		bodyOrbit = new Sprite(new Texture(Gdx.files.internal("paths/orbit_body.png")));
		
		bgTimer = new Vector();
		plCtr_scr = new Vector();
		scr_offset = new Vector(screen.x/2, screen.y/2);
		
		orbit = new Sprite(new Texture(Gdx.files.internal("paths/orbit.png")));
		/* Randomly generate starry background
		for(float pos_x = (float) (screen.x*-0.2071); pos_x < screen.x*1.2071; pos_x++) {
			int layer = (int) (Math.random()*3);
			float probFactor = 0;
			switch(layer) {
				case 0:
						probFactor = 0.5f;
						break;
				case 1:
						probFactor = 0.7f;
						break;
				case 2:
						probFactor = 0.9f;
						break;
			}
			
			if(Math.random() > probFactor) {
				Vector location = new Vector(pos_x - (screen.x/2), (float) ((Math.random()*screen.x*1.4142) - (screen.x*0.7071)));
				bgStars.add(new BackgroundStar(starSprite, layer, location));
			}
		} */
		location = new Vector();
		relPos = new Vector();
		relVel = new Vector();
		radius = 0;
		orbPos = new Vector();
	}
	
	public PropertyList getCurrentData() {
		PropertyList data = new PropertyList();
		// Vector location, Vector speed, float heading, float fuelUnits
		for(Star star: stars) {
			for(Planet planet: star.planets) {
				data.setProp(star.name + "/" + planet.name + "/theta", planet.theta);
				for(Moon moon: planet.moons) {
					data.setProp(star.name + "/" + planet.name + "/" + moon.name + "/theta", moon.theta);
				}
			}
		}
		return data;
	}
	
	public void loadSavedData(PropertyList data) {
		for(Star star: stars) {
			for(Planet planet: star.planets) {
				planet.theta = data.getFloat(star.name + "/" + planet.name + "/theta");
				for(Moon moon: planet.moons) {
					moon.theta = data.getFloat(star.name + "/" + planet.name + "/" + moon.name + "/theta");
				}
			}
		}
	}
	
	public void process(Spacecraft spacecraft, float dt, float scale, float timeWarp, boolean calcForce) {	
		
		// Process revolution cycles
		for(Star star: this.stars) {
			star.process(dt*timeWarp);
			// Get Absolute <x,y> positions from relative angles and update
			for(Planet planet: star.planets) {
				// Update Planet Positions
				planet.location = Vector.add(star.location, Vector.polar2Rect(planet.revRadius, planet.theta));
				for(Moon moon: planet.moons) {
					// Update Moon Positions
					moon.location = Vector.add(planet.location, Vector.polar2Rect(moon.revRadius, moon.theta));
				}
			}
		}
		
		/* Process Background
		if(bgStars.size() > 0) {
			for(BackgroundStar bgStar: bgStars) {
				if((bgStar != null)) {
					bgStar.update(spacecraft.absVel, spacecraft.heading, scale, timeWarp);
				}
			}
		} */
		
		// Process angular motion
		spacecraft.heading += spacecraft.omega*dt;
		
		// Process planetary physics
		
		e_min = 2;
		e_minPl = 2;
		e_minMn = 2;
		e_minSt = 2;
		
		// Run gravitational calculations
		for(Star star: this.stars) {
			
			// Relative position and velocity of spacecraft
			Vector relPos = Vector.subtract(spacecraft.location, star.location);
			Vector relVel = spacecraft.absVel;
			// Gravitational Parameter
			mu = G*star.mass;
			// Calculate Eccentricity
			e = (float) Math.sqrt(1 + (((2*Math.pow(relPos.getMagnitude()*relVel.getMagnitude()*Math.sin(Vector.angleBetween(relPos, relVel)), 2))/(Math.pow(mu, 2)))*(((Math.pow(relVel.getMagnitude(),2))/2)-(mu/relPos.getMagnitude()))));
			
			for(Planet planet: star.planets) {
				
				// Relative position and velocity of spacecraft
				Vector relPos_pl = Vector.subtract(spacecraft.location, planet.location);
				Vector vel_planet = Vector.polar2Rect((float) ((2*Math.PI*planet.revRadius)/planet.orbPeriod), (float) (planet.location.getDirection() + (Math.PI/2)));
				// Vector vel_planet = Vector.multiplyScalar(Vector.subtract(planet.location, planet.lastLoc), timeWarp/dt);
				Vector relVel_pl = Vector.subtract(spacecraft.absVel, vel_planet);
				// Vector relVel_pl = spacecraft.speed;
				// Gravitational Parameter
				mu_pl = G*planet.mass;
				// Calculate Eccentricity
				e_pl = (float) Math.sqrt(1 + (((2*Math.pow(relPos_pl.getMagnitude()*relVel_pl.getMagnitude()*Math.sin(Vector.angleBetween(relPos_pl, relVel_pl)), 2))/(Math.pow(mu_pl, 2)))*(((Math.pow(relVel_pl.getMagnitude(),2))/2)-(mu_pl/relPos_pl.getMagnitude()))));
				
				for(Moon moon: planet.moons) {
					
					// Relative position and velocity of spacecraft
					Vector relPos_mn = Vector.subtract(spacecraft.location, moon.location);
					Vector vel_moon = Vector.add(vel_planet, Vector.polar2Rect((float) ((2*Math.PI*moon.revRadius)/moon.orbPeriod), (float) (Vector.subtract(moon.location, planet.location).getDirection() + (Math.PI/2))));
					Vector relVel_mn = Vector.subtract(spacecraft.absVel, vel_moon);
					// Vector relVel_pl = spacecraft.speed;
					// Gravitational Parameter
					mu_mn = G*moon.mass;
					// Calculate Eccentricity
					e_mn = (float) Math.sqrt(1 + (((2*Math.pow(relPos_mn.getMagnitude()*relVel_mn.getMagnitude()*Math.sin(Vector.angleBetween(relPos_mn, relVel_mn)), 2))/(Math.pow(mu_mn, 2)))*(((Math.pow(relVel_mn.getMagnitude(),2))/2)-(mu_mn/relPos_mn.getMagnitude()))));
					
					if((e_mn < e_minMn)) {
						e_minMn = e_mn;
						obcMoon = moon;
						obcMoon.speed = vel_moon;
					}
					
				}
				
				if((e_pl < e_minPl)) {
					e_minPl = e_pl;
					obcPlanet = planet;
					obcPlanet.speed = vel_planet;
				}
		
			}
			
			if(e < e_minSt) {
				e_minSt = e;
				obcStar = star;
				obcStar.speed.setXY(0, 0);
			}
			
		}
		
		if(e_minMn < 1) {
			e_min = e_minMn;
			orbitalCenter = obcMoon;
			orbitalCenter.e = e_min;
			if(!spacecraft.refBody.equals(orbitalCenter.name)) {
				spacecraft.speed = Vector.subtract(spacecraft.absVel, orbitalCenter.speed);
				spacecraft.refBody = orbitalCenter.name;
				spacecraft.relPos = Vector.subtract(spacecraft.location, orbitalCenter.location);
			}
			drawOrbit = true;
		} else if(e_minPl <= 1) {
			e_min = e_minPl;
			orbitalCenter = obcPlanet;
			if(!spacecraft.refBody.equals(orbitalCenter.name)) {
				spacecraft.speed = Vector.subtract(spacecraft.absVel, orbitalCenter.speed);
				spacecraft.refBody = orbitalCenter.name;
				spacecraft.relPos = Vector.subtract(spacecraft.location, orbitalCenter.location);
			}
			orbitalCenter.e = e_min;
			drawOrbit = true;
		} else if(e_minSt <= 1) {
			e_min = e_minSt;
			orbitalCenter = obcStar;
			if(!spacecraft.refBody.equals(orbitalCenter.name)) {
				spacecraft.speed = spacecraft.absVel;
				spacecraft.refBody = orbitalCenter.name;
				spacecraft.relPos = Vector.subtract(spacecraft.location, orbitalCenter.location);
			}
			orbitalCenter.e = e_min;
			drawOrbit = true;
		} else {
			drawOrbit = false;
			orbitalCenter = null;
		}
		
		Vector a_net = new Vector(0,0);
		
		if(calcForce) {
			if((e_min <= 1) && (orbitalCenter != null)) {				
				
				// Get gravitational acceleration due to influential body
				
				a_net = Vector.polar2Rect((float) ((G*orbitalCenter.mass)/Math.pow(spacecraft.relPos.getMagnitude(), 2)), Vector.multiplyScalar(spacecraft.relPos, -1).getDirection());
				
				spacecraft.speed = Vector.add(spacecraft.speed, Vector.multiplyScalar(a_net, dt*timeWarp));
				spacecraft.absVel = Vector.add(spacecraft.speed, orbitalCenter.speed);
				spacecraft.relPos = Vector.add(spacecraft.relPos, Vector.multiplyScalar(spacecraft.speed, dt*timeWarp));
				spacecraft.location = Vector.add(orbitalCenter.location, spacecraft.relPos);
				
			} else {
				
				// Super-position influence from all celestial bodies
				
				// Run gravitational calculations
				for(Star star: this.stars) {
					float dist2star = spacecraft.location.getDistanceTo(star.location);
					a_net = Vector.add(a_net, Vector.polar2Rect((float) ((G*star.mass)/(dist2star*dist2star)), Vector.subtract(star.location, spacecraft.location).getDirection()));	
					for(Planet planet: star.planets) {
						float dist2planet = spacecraft.location.getDistanceTo(planet.location);
						a_net = Vector.add(a_net, Vector.polar2Rect((float) ((G*planet.mass)/(dist2planet*dist2planet)), Vector.subtract(planet.location, spacecraft.location).getDirection()));
						for(Moon moon: planet.moons) {
							float dist2moon = spacecraft.location.getDistanceTo(moon.location);
							a_net = Vector.add(a_net, Vector.polar2Rect((float) ((G*moon.mass)/(dist2moon*dist2moon)), Vector.subtract(moon.location, spacecraft.location).getDirection()));
						}
					}	
				}
				
				spacecraft.absVel = Vector.add(spacecraft.absVel, Vector.multiplyScalar(a_net, dt*timeWarp));
				spacecraft.location = Vector.add(spacecraft.location, Vector.multiplyScalar(spacecraft.absVel, dt*timeWarp));
				
			}
		}
		
		// Process Background scrolling
		bgTimer = Vector.add(bgTimer, new Vector((float) ((spacecraft.absVel.x/(1E11/timeWarp))*dt), (float) (spacecraft.absVel.y/(1E11/timeWarp))*dt));
		bg1Sprite.setU(bgTimer.x);
		bg1Sprite.setU2(bgTimer.x+3);
		bg1Sprite.setV2(bgTimer.y);
		bg1Sprite.setV(bgTimer.y+3);
		bg2Sprite.setU(bgTimer.x);
		bg2Sprite.setU2(bgTimer.x+2);
		bg2Sprite.setV2(bgTimer.y);
		bg2Sprite.setV(bgTimer.y+2);
		bg3Sprite.setU(bgTimer.x);
		bg3Sprite.setU2(bgTimer.x+1);
		bg3Sprite.setV2(bgTimer.y);
		bg3Sprite.setV(bgTimer.y+1);
		
	}
	
	public CelestialBody render(SpriteBatch batch, Spacecraft spacecraft, Vector screen, float scale, float timeWarp, Vector touch, boolean showInfoBox, CelestialBody infoBody, Vector infoBoxPos, float clickTimer) {
		
		// Draw and control background
		
		batch.draw(bg1Sprite, (float) (-screen.x*0.2071), (float) ((screen.y/2) - (screen.x*0.7071)),(float) (screen.x*0.70711), (float) (screen.x*0.70711), (float) (screen.x*1.414216), (float) (screen.x*1.414216), 1, 1, spacecraft.heading*Vector.RAD2DEG);
		batch.draw(bg2Sprite, (float) (-screen.x*0.2071), (float) ((screen.y/2) - (screen.x*0.7071)),(float) (screen.x*0.70711), (float) (screen.x*0.70711), (float) (screen.x*1.414216), (float) (screen.x*1.414216), 1, 1, spacecraft.heading*Vector.RAD2DEG);
		batch.draw(bg3Sprite, (float) (-screen.x*0.2071), (float) ((screen.y/2) - (screen.x*0.7071)),(float) (screen.x*0.70711), (float) (screen.x*0.70711), (float) (screen.x*1.414216), (float) (screen.x*1.414216), 1, 1, spacecraft.heading*Vector.RAD2DEG);
		
		/* Draw and move background
		if(bgStars.size() > 0) {
			for(BackgroundStar bgStar: bgStars) {
				if((bgStar != null)) {
					bgStar.render(batch, screen);
					if(bgStar.location.getMagnitude() > screen.x*0.7071) {
						bgStar = null;
					} 
				}
			}
		}
		
		// Randomly generate starry background
		layer = (int) (Math.random()*3);
		probFactor = 0;
		switch(layer) {
			case 0:
					probFactor = 10/(spacecraft.absVel.getMagnitude()/200);
					break;
			case 1:
					probFactor = 35/(spacecraft.absVel.getMagnitude()/200);
					break;
			case 2:
					probFactor = 100/(spacecraft.absVel.getMagnitude()/200);
					break;
		}
		
		if(Math.random() > probFactor) {
			posHdg = (float) ((spacecraft.absVel.getDirection()*Vector.RAD2DEG) + (Math.random()*180 - 90));
			location.setPolar((float) (screen.x*0.7071), posHdg*Vector.DEG2RAD);
			bgStars.add(new BackgroundStar(starSprite, layer, location));
		} */
		
		// Plot Orbit
		if(drawOrbit) {
			Vector relPos = spacecraft.relPos;
			Vector relVel = spacecraft.speed;
			r = relPos.getMagnitude();
			v = relVel.getMagnitude();
			mu = G*orbitalCenter.mass;
			phi = Vector.angleBetween(relPos, relVel);
			beta = relPos.getDirection();
			
			// Calculate Semi-latus-rectum
			p = (float) ((Math.pow(r*v*Math.sin(phi), 2))/mu);
			
			bugfix = 0;
			
			// Calculate true anomaly
			th_factor = (p-r)/(r*orbitalCenter.e);
			
			if(th_factor > 1) {
				th_factor = 1;
			} else if(th_factor < -1) {
				th_factor = -1;
			}
			
			theta = (float) Math.acos(th_factor);
			
			if(Math.abs(phi) > Math.PI/2) {
				theta = -theta;
			}
			
			if(Vector.cross(relPos, relVel) < 0) {
				theta = -theta;
			}
			
			// Predict and draw orbital path
			for(float gamma = 0; gamma < 2*Math.PI; gamma += Math.PI/(100)) {
				radius = (float) ((p/(1 + (orbitalCenter.e*Math.cos(beta - theta - gamma + spacecraft.heading)))));
				orbPos = new Vector((float) (screen.x/2 + ((radius*Math.cos(gamma) - Vector.polar2Rect(spacecraft.relPos.getMagnitude(), spacecraft.relPos.getDirection() + spacecraft.heading).x)*(screen.x/(200000000*scale)))), (float) (screen.y/2 + ((radius*Math.sin(gamma) - Vector.polar2Rect(spacecraft.relPos.getMagnitude(), spacecraft.relPos.getDirection() + spacecraft.heading).y)*(screen.x/(200000000*scale)))));
				if((orbPos.x > 0) && (orbPos.x < screen.x) && (orbPos.y > 0) && (orbPos.y < screen.y)) {
					batch.draw(orbit, orbPos.x, orbPos.y, screen.x/160, screen.x/160);
				}
			}
		}
		
		returnBody = null;
		
		// Draw Celestial Bodies
		for(Star star: this.stars) {
			star.lastLoc = star.location;
			for(Planet planet: star.planets) {
				planet.lastLoc = planet.location;
				for(Moon moon: planet.moons) {
					if(Math.abs(moon.location.getMagnitude() - spacecraft.location.getMagnitude()) < (scale*2*400000000)) {
						if((moon.revRadius/scale <= 2E8) && (moon.revRadius/scale >= 1.2E7)) {
							batch.draw(bodyOrbit, (planet.location.x - spacecraft.location.x - moon.revRadius)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (planet.location.y - spacecraft.location.y - moon.revRadius)*(screen.x/(200000000*scale)),-(planet.location.x - spacecraft.location.x - moon.revRadius)*(screen.x/(200000000*scale)),-(planet.location.y - spacecraft.location.y - moon.revRadius)*(screen.x/(200000000*scale)), (moon.revRadius*2)*(screen.x/(200000000*scale)), (moon.revRadius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
						}
						if((moon.radius*2)*(screen.x/(200000000*scale)) > 12) {
							batch.draw(moon.sprite, (moon.location.x - spacecraft.location.x - moon.radius)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (moon.location.y - spacecraft.location.y - moon.radius)*(screen.x/(200000000*scale)),-(moon.location.x - spacecraft.location.x - moon.radius)*(screen.x/(200000000*scale)),-(moon.location.y - spacecraft.location.y - moon.radius)*(screen.x/(200000000*scale)), (moon.radius*2)*(screen.x/(200000000*scale)), (moon.radius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
						} else {
							batch.draw(moon.sprite, (moon.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (screen.x/2) - (3*(screen.x/960)), (screen.y/2) + (moon.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) - (3*(screen.x/960)),-(moon.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (3*(screen.x/960)),-(moon.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) + (3*(screen.x/960)), (6*(screen.x/960)), (6*(screen.x/960)),1,1,spacecraft.heading*Vector.RAD2DEG);
						}
						if((clickTimer > 0.25f) && Gdx.input.isTouched() && (!showInfoBox || (!infoBoxPos.equals(touch)))) {
							plCtr_scr = Vector.add(Vector.polar2Rect(Vector.subtract(moon.location, spacecraft.location).getMagnitude()*(screen.x/(200000000*scale)), Vector.subtract(moon.location, spacecraft.location).getDirection() + spacecraft.heading), scr_offset);
							plRad_scr = moon.radius*(screen.x/(200000000*scale));
							if(Bounds.circle(plCtr_scr, touch, plRad_scr + screen.x/32)) {
								returnBody = moon;
							}
						}
					}
					moon.lastLoc = moon.location;
				}
				if(Math.abs(planet.location.getMagnitude() - spacecraft.location.getMagnitude()) < (scale*2*400000000)) {
					if((planet.revRadius/scale <= 2E8) && (planet.revRadius/scale >= 1.2E7)) {
						batch.draw(bodyOrbit, (star.location.x - spacecraft.location.x - planet.revRadius)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (star.location.y - spacecraft.location.y - planet.revRadius)*(screen.x/(200000000*scale)),-(star.location.x - spacecraft.location.x - planet.revRadius)*(screen.x/(200000000*scale)),-(star.location.y - spacecraft.location.y - planet.revRadius)*(screen.x/(200000000*scale)), (planet.revRadius*2)*(screen.x/(200000000*scale)), (planet.revRadius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
					}
					if((planet.radius*2)*(screen.x/(200000000*scale)) > 18) {
						if(planet.hasRings) {
							batch.draw(planet.sprite, (planet.location.x - spacecraft.location.x - (planet.radius*2))*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (planet.location.y - spacecraft.location.y - planet.radius)*(screen.x/(200000000*scale)),-(planet.location.x - spacecraft.location.x - (planet.radius*2))*(screen.x/(200000000*scale)),-(planet.location.y - spacecraft.location.y - planet.radius)*(screen.x/(200000000*scale)), (planet.radius*4)*(screen.x/(200000000*scale)), (planet.radius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
						} else {
							batch.draw(planet.atmos, (planet.location.x - spacecraft.location.x - planet.radWithAtmos)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (planet.location.y - spacecraft.location.y - planet.radWithAtmos)*(screen.x/(200000000*scale)),-(planet.location.x - spacecraft.location.x - planet.radWithAtmos)*(screen.x/(200000000*scale)),-(planet.location.y - spacecraft.location.y - planet.radWithAtmos)*(screen.x/(200000000*scale)), (planet.radWithAtmos*2)*(screen.x/(200000000*scale)), (planet.radWithAtmos*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
							batch.draw(planet.sprite, (planet.location.x - spacecraft.location.x - planet.radius)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (planet.location.y - spacecraft.location.y - planet.radius)*(screen.x/(200000000*scale)),-(planet.location.x - spacecraft.location.x - planet.radius)*(screen.x/(200000000*scale)),-(planet.location.y - spacecraft.location.y - planet.radius)*(screen.x/(200000000*scale)), (planet.radius*2)*(screen.x/(200000000*scale)), (planet.radius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
						}
					} else {
						if(planet.hasRings) {
							batch.draw(planet.sprite, (planet.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (screen.x/2) - (12*(screen.x/960)), (screen.y/2) + (planet.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) - (6*(screen.x/960)),-(planet.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (12*(screen.x/960)),-(planet.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) + (6*(screen.x/960)), (24*(screen.x/960)), (12*(screen.x/960)),1,1,spacecraft.heading*Vector.RAD2DEG);
						} else {
							batch.draw(planet.sprite, (planet.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (screen.x/2) - (6*(screen.x/960)), (screen.y/2) + (planet.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) - (6*(screen.x/960)),-(planet.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (6*(screen.x/960)),-(planet.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) + (6*(screen.x/960)), (12*(screen.x/960)), (12*(screen.x/960)),1,1,spacecraft.heading*Vector.RAD2DEG);
						}
					}
					if((clickTimer > 0.25f) && Gdx.input.isTouched() && (!showInfoBox || (!infoBoxPos.equals(touch)))) {
						plCtr_scr = Vector.add(Vector.polar2Rect(Vector.subtract(planet.location, spacecraft.location).getMagnitude()*(screen.x/(200000000*scale)), Vector.subtract(planet.location, spacecraft.location).getDirection() + spacecraft.heading), scr_offset);
						plRad_scr = planet.radius*(screen.x/(200000000*scale));
						if(Bounds.circle(plCtr_scr, touch, plRad_scr + screen.x/32)) {
							returnBody = planet;
						}
					}
				}
			}
			if(Math.abs(star.location.getMagnitude() - spacecraft.location.getMagnitude()) < (scale*2*400000000)) {
				if((star.radius*2)*(screen.x/(200000000*scale)) > 24) {
					batch.draw(star.sprite, (star.location.x - spacecraft.location.x - star.radius)*(screen.x/(200000000*scale)) + (screen.x/2), (screen.y/2) + (star.location.y - spacecraft.location.y - star.radius)*(screen.x/(200000000*scale)),-(star.location.x - spacecraft.location.x - star.radius)*(screen.x/(200000000*scale)),-(star.location.y - spacecraft.location.y - star.radius)*(screen.x/(200000000*scale)), (star.radius*2)*(screen.x/(200000000*scale)), (star.radius*2)*(screen.x/(200000000*scale)),1,1,spacecraft.heading*Vector.RAD2DEG);
				} else {
					batch.draw(star.sprite, (star.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (screen.x/2) - (12*(screen.x/960)), (screen.y/2) + (star.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) - (12*(screen.x/960)),-(star.location.x - spacecraft.location.x)*(screen.x/(200000000*scale)) + (12*(screen.x/960)),-(star.location.y - spacecraft.location.y)*(screen.x/(200000000*scale)) + (12*(screen.x/960)), (24*(screen.x/960)), (24*(screen.x/960)),1,1,spacecraft.heading*Vector.RAD2DEG);
				}
			}
		}
	
		return returnBody;
		
	}

}
