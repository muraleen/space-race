// SpaceRace: Moon Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Moon extends CelestialBody {
	
	public Moon(String name, float mass, float revRadius, float orbPeriod, float radius, float theta) {
		this.mass = mass;
		this.revRadius = revRadius*1000;
		this.orbPeriod = orbPeriod*86400;
		this.radius = radius*1000;
		this.theta = (float) (theta*Vector.DEG2RAD);
		this.name = name;
		this.sprite = new Sprite(new Texture(Gdx.files.internal("bodies/"+name+".png")));
		this.type = "moon";
	}
	
}
