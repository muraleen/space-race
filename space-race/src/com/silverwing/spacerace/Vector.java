// SpaceRace: Vector Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;


public class Vector {
	
	public float x, y, z;
	public static float DEG2RAD = (float) 0.0174532925, RAD2DEG = (float) 57.2957795;
	public boolean is2D;
	
	// Vector class constructor: <0,0>
	public Vector() {
		this.x = 0;
		this.y = 0;
		this.z = 0;
		this.is2D = true;
	}
	
	// Vector class constructor: <x,x>
	public Vector(float i) {
		this.x = i;
		this.y = i;
		this.z = 0;
		this.is2D = true;
	}
	
	// Vector class constructor: <x,y>
	public Vector(float i, float j) {
		this.x = i;
		this.y = j;
		this.z = 0;
		this.is2D = true;
	}
	
	// Input polar vector coordinates (RAD)
	public void setPolar(float r, float theta) {
		this.x = (float) (r*Math.cos(theta));
		this.y = (float) (r*Math.sin(theta));
	}
	
	// Get polar coordinates for vector components (RAD)
	public Vector getPolar() {
		return new Vector(this.getMagnitude(), this.getDirection());	
	}
	
	// Update vector coordinates
	public void setXY(float d, float e) {
		this.x = d;
		this.y = e;
		this.is2D = true;
	}
	
	// Return Magnitude of the vector
	public float getMagnitude() {
		return (float) Math.sqrt(Math.pow(x,2) + Math.pow(y, 2));
	}
	
	// Return Direction of the vector from the origin [RADIANS]
	public float getDirection() {
		return (float) Math.atan2(y,x);
	}
	
	// Return course to argument vector [RADIANS]
	public float getCourseTo(Vector that) {
		return (float) Math.atan2(that.y - this.y,that.x - this.x);
	}
	
	// Return Distance from this vector to the argument vector
	public float getDistanceTo(Vector that) {
		return (float) Math.sqrt(Math.pow(that.x - this.x,2) + Math.pow(that.y - this.y, 2));
	}
	
	// Return Vector with new coordinates
	public Vector setCrsDist(float crs, float dist) {
		Vector vector = new Vector(this.x, this.y);
		vector.x += dist * Math.cos(crs);
		vector.y += dist * Math.sin(crs);
		return vector;
	}
	
	// Get Unit Vector
	public Vector getUnitVector() {
		return new Vector(this.x/this.getMagnitude(), this.y/this.getMagnitude());
	}
	
	// Get angle between vectors
	public static float angleBetween(Vector a, Vector b) {
		return (float) Math.acos((Vector.dot(a, b)/(a.getMagnitude()*b.getMagnitude())));
	}
	
	// 2D Vector Arithmetic Operations
	
	// Vector Addition
	public static Vector add(Vector a, Vector b) {
		return new Vector(a.x + b.x, a.y + b.y);
	}
	
	// Vector Subtraction
	public static Vector subtract(Vector a, Vector b) {
		return new Vector(a.x - b.x, a.y - b.y);
	}
	
	// Scalar Multiplication
	public static Vector multiplyScalar(Vector a, float k) {
		return new Vector(a.x*k, a.y*k);
	}
	
	// Polar to Rect
	public static Vector polar2Rect(float r, float theta) {
		Vector vect = new Vector();
		vect.setPolar(r, theta);
		return vect;
	}
	
	// Dot Product
	public static float dot(Vector a, Vector b) {
		return (a.x*b.x) + (a.y*b.y);
	}
	
	// Cross Product (2D)
	public static float cross(Vector a, Vector b) {
		return ((a.x*b.y) - (b.x*a.y));
	}
	
	// Set Vector but don't connect
	public void setVect(Vector a) {
		this.x = a.x;
		this.y = a.y;
	}
	
	// Check if it equals to something
	public boolean equals(Vector a) {
		if((this.x == a.x) && (this.y == a.y)) {
			return true;
		} else {
			return false;
		}
	}

}
