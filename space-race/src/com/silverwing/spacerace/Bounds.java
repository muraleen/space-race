// SpaceRace: Bounds Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

public class Bounds {

	public static boolean circle(Vector center, Vector point, double radius) {
		double distFromCenter = Math.sqrt(Math.pow(point.x - center.x, 2) + Math.pow(point.y - center.y, 2));
		if(distFromCenter <= radius) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean square(Vector bottomLeft, Vector size, Vector point) {
		if((point.x > bottomLeft.x) && (point.x < (bottomLeft.x + size.x)) && (point.y > bottomLeft.y) && (point.y < (bottomLeft.y + size.y))) {
			return true;
		} else {
			return false;
		}
	}
	
}
