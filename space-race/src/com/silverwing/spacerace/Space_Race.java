// SpaceRace: SpaceRace Main libGDX Program File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public class Space_Race implements ApplicationListener, InputProcessor {
	
	public int gameMode = 2;
	
	private SpriteBatch batch;
	public CelestialBody navWpt;
	public boolean navSet = false;
	public Vector screen;
	public Spacecraft spacecraft;
	public Universe2D universe;
	public int nFingers = 0, finger1_ptr, finger2_ptr;
	public float lastDistance = 0, scaleFactor = 1;
	public Vector3 finger1 = new Vector3();
	public Vector3 finger2 = new Vector3();
	public BitmapFont fnt_moonhouse;
	public CharSequence speed, msg, altitude, dtg, eta, distToGo;
	public String orbCenter, destination;
	public float MPS2MPH = 2.23694f;
	public Button_2State timeWarp_slow, timeWarp_fast, zoomIn, zoomOut, nav_target;
	float clickTimer = 0f;
	public Timer timer;
	public int fontSize = 32;
	public float missionDuration = 0, widest, infoBoxHeight, infoScale;
	float dt;
	public Vector touch;
	public Sprite dpad, infobox, closebtn, btnEdge;
	public ArrayList<String> dispOrder = new ArrayList<String>();
	public boolean showInfoBox, infoBoxinit = false, noClick = true;
	public CelestialBody infoBody, returnBody;
	Vector infoBoxPos = new Vector();
	Vector infoBoxOffset = new Vector();
	Vector closeBtnPos = new Vector();
	Vector scr_offset;
	
	// Stuff specific for launch/landing screen [relative parameters]
	public float alt = 0, roll = 0;
	public Lander lander;
	
	// Scaling Factors
	float scale[] = {0.015625f, 0.03125f, 0.0625f, 0.125f, 0.25f, 0.5f, 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000, 5000, 10000, 50000, 100000};
	int scaleIndex = 3; // Start at Scale 0.125
	float scaleValue = 0.125f;
	
	// Time Warp Factors
	float timeWarp[] = {0, 1, 2, 5, 10, 25, 50, 100, 250, 500, 1000, 2500, 5000, 10000, 20000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 50000000};
	int timeWarpIndex = 0; // Start without time warp
	
	@Override
	public void create() {
		screen = new Vector();
		touch = new Vector();
		screen.setXY(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch = new SpriteBatch();
		infobox = new Sprite(new Texture(Gdx.files.internal("overlay/infobox.png")));
		closebtn = new Sprite(new Texture(Gdx.files.internal("overlay/closebtn.png")));
		btnEdge = new Sprite(new Texture(Gdx.files.internal("overlay/btn_edge.png")));
		
		if(screen.x >= 1920) {
			fontSize = 48;
		} else if(screen.x >= 1440) {
			fontSize = 32;
		} else {
			fontSize = 24;
		}
		
		fnt_moonhouse = new BitmapFont(Gdx.files.internal("fonts/moonhouse"+Integer.toString(fontSize)+".fnt"), false);
		dpad = new Sprite(new Texture(Gdx.files.internal("buttons/dpad.png")));
		
		speed = "SPD: 0 KM/S";
		
		timeWarp_slow = new Button_2State(new Vector(screen.x-(4*(screen.x/16))-(screen.x/64), screen.y - (5*screen.x)/64), new Vector(screen.x/16,screen.x/16), "buttons/timeWarp-.png");
		timeWarp_fast = new Button_2State(new Vector(screen.x-(3*(screen.x/16))-(screen.x/64), screen.y - (5*screen.x)/64), new Vector(screen.x/16,screen.x/16), "buttons/timeWarp+.png");
		zoomOut = new Button_2State(new Vector(screen.x-(2*(screen.x/16))-(screen.x/64), screen.y - (5*screen.x)/64), new Vector(screen.x/16,screen.x/16), "buttons/zoomOut.png");
		zoomIn = new Button_2State(new Vector(screen.x-(1*(screen.x/16))-(screen.x/64), screen.y - (5*screen.x)/64), new Vector(screen.x/16,screen.x/16), "buttons/zoomIn.png");
		
		// Read SpaceRace Data using SRDataInterface
		
		// Initialize planetary physics engine and space craft
		universe = new Universe2D(SRDataInterface.getCelestialBodies(), screen);
		
		// Load saved data if exists
		if(Gdx.files.local("universe.dat").exists()) {
			try {
				PropertyList localData = LocalData.readData("universe.dat");
				universe.loadSavedData(localData);
				} catch (ClassNotFoundException e) {
				System.out.println("[ERROR] Class Not Found Exception!");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("[ERROR] IO Exception!");
				e.printStackTrace();
			}
		}
		
		if(Gdx.files.local("spacecraft.dat").exists()) {
			try {
				PropertyList localData = LocalData.readData("spacecraft.dat");
				System.out.println("Loading saved data...");
				spacecraft = new Spacecraft("fox9", screen, new Vector(localData.getFloat("location-x"),localData.getFloat("location-y")), new Vector(localData.getFloat("speed-x"),localData.getFloat("speed-y")), localData.getFloat("heading"), localData.getFloat("fuel-units"), new Vector(screen.x-(screen.x/8)-(screen.x/64), (screen.x/64) + (screen.x/8)), screen.x/6);
			} catch (ClassNotFoundException e) {
				System.out.println("[ERROR] Class Not Found Exception!");
				spacecraft = new Spacecraft("fox9", screen, new Vector(0,0), new Vector(-29000,0), 0, 12000, new Vector(screen.x-(screen.x/8)-(screen.x/64), (screen.x/64) + (screen.x/8)), screen.x/6);
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("[ERROR] IO Exception!");
				spacecraft = new Spacecraft("fox9", screen, new Vector(0,0), new Vector(-29000,0), 0, 12000, new Vector(screen.x-(screen.x/8)-(screen.x/64), (screen.x/64) + (screen.x/8)), screen.x/6);
				e.printStackTrace();
			}
		} else {
			spacecraft = new Spacecraft("fox9", screen, new Vector(0,0), new Vector(-29000,0), 0, 12000, new Vector(screen.x-(screen.x/8)-(screen.x/64), (screen.x/64) + (screen.x/8)), screen.x/6);	
		}
		
		universe.process(spacecraft, Gdx.graphics.getDeltaTime(), scale[scaleIndex], timeWarp[timeWarpIndex], false);
		timer = new Timer();
		msg = "";
		scr_offset = new Vector(screen.x/2, screen.y/2);
		
		// Initialize navigation waypoint
		navWpt = universe.stars.get(0).planets.get(2).moons.get(0);
		
		nav_target = new Button_2State(new Vector(0,0), new Vector(0,0), "overlay/btn_mid.png");
		
		// Initialize Lander data
		lander = new Lander("spacecraft");
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		try {
			LocalData.saveData(spacecraft.getCurrentData(), "spacecraft.dat");
			System.out.println("Saving Spacecraft data...");
		} catch (IOException e) {
			System.out.println("[ERROR] IO Exception!");
			e.printStackTrace();
		}
		try {
			LocalData.saveData(universe.getCurrentData(), "universe.dat");
			System.out.println("Saving Universe data...");
		} catch (IOException e) {
			System.out.println("[ERROR] IO Exception!");
			e.printStackTrace();
		}
	}

/*############################################ START OF RENDER METHOD ################################################*/
	@Override
	public void render() {		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);		// Run main update code
		
		// Update/Process
		dt = Gdx.graphics.getDeltaTime();
		
		// Handle clicks or presses
		touch.setXY(Gdx.input.getX(), screen.y - Gdx.input.getY());
		
		switch(gameMode) {
		
			case 0: // SPLASH SCREEN
				
				
				
				
				break;
/*####################################################################################################################*/
			case 1: // MENU SCREEN
				
				
				
				
				break;
/*####################################################################################################################*/
			case 2: // ORBITAL SCREEN
		
				// Update mission duration
				missionDuration += dt*timeWarp[timeWarpIndex];
				
				noClick = true;
				if(timeWarp_slow.isClicked(touch)) {
					if((clickTimer > 0.2)) { // || showInfoBox) {
						if(timeWarpIndex > 0) timeWarpIndex--;
						msg = "Time Warp: " + (int) timeWarp[timeWarpIndex] + "x";
						timer.cancel();
						timer = new Timer();
						timer.schedule(new TimerTask() {
							@Override
							public void run() {
								msg = "";
							}	
						}, 2000);
						clickTimer = 0f;
					}
					noClick = false;
				} else if(timeWarp_fast.isClicked(touch)) {
					if((clickTimer > 0.2)) {
						if(timeWarpIndex < timeWarp.length-1) timeWarpIndex++;
						msg = "Time Warp: " + (int) timeWarp[timeWarpIndex] + "x";
						timer.cancel();
						timer = new Timer();
						timer.schedule(new TimerTask() {
							@Override
							public void run() {
								msg = "";
							}	
						}, 2000);
						clickTimer = 0f;
					}
					noClick = false;
				} else if(zoomOut.isClicked(touch)) {
					if((clickTimer > 0.2)) {
						if(scaleIndex < scale.length-1) scaleIndex++;
						clickTimer = 0f;
					}
					noClick = false;
				} else if(zoomIn.isClicked(touch)) {
					if((clickTimer > 0.2)) {	
						if(scaleIndex > 0) scaleIndex--;
						clickTimer = 0f;
					}
					noClick = false;
				} else {
					noClick = spacecraft.control(screen, touch, dt, timeWarp[timeWarpIndex], this);
				}
				
				clickTimer += Gdx.graphics.getDeltaTime();
				
				scaleValue = approachExpo(scaleValue, scale[scaleIndex], 8);
				
				universe.process(spacecraft, dt, scaleValue, timeWarp[timeWarpIndex], true);
				batch.begin();
				
				// Render to Sprite batch
		
				
				// Check if an object is clicked on
				
				returnBody = universe.render(batch, spacecraft, screen, scaleValue, timeWarp[timeWarpIndex], touch, showInfoBox, infoBody, infoBoxPos, clickTimer);
				if((returnBody != null) && noClick) {
					showInfoBox = true;
					infoBody = returnBody;
					infoBoxOffset = Vector.subtract(touch,Vector.add(Vector.polar2Rect(Vector.subtract(infoBody.location, spacecraft.location).getMagnitude()*(screen.x/(200000000*scaleValue)), Vector.subtract(infoBody.location, spacecraft.location).getDirection() + spacecraft.heading), scr_offset));
					infoScale = scaleValue;
					infoBoxinit = false;
				}
				spacecraft.render(batch, screen, scaleValue, navWpt.location, navSet, universe.drawOrbit);
				
				// System.out.println("Distance to Earth: " + (spacecraft.location.getDistanceTo(universe.stars.get(0).planets.get(2).location ) - universe.stars.get(0).planets.get(2).radius)/1000 + " KM\t\tDistance to Moon: " + (spacecraft.location.getDistanceTo(universe.stars.get(0).planets.get(2).moons.get(0).location) - universe.stars.get(0).planets.get(2).moons.get(0).radius)/1000 + " KM");
				
				// Write Data on screen
				speed = "SPD: " + String.format("%.2f", spacecraft.speed.getMagnitude()/1000) + " KM/S";
				
				
				if(navSet) {
					fnt_moonhouse.setColor(0.82f, 0, 1, 1);
					fnt_moonhouse.draw(batch, "NAV > " + String.valueOf(navWpt.name).toUpperCase(),(float) (screen.x*0.02), (float) ((float) (screen.y) - (1.25*fontSize)));
				}
				
				fnt_moonhouse.setColor(0.075f, 0.663f, 0.102f, 1);
		
				if(navSet) {
					// Navigation WPT Information
					if((Vector.subtract(spacecraft.location, navWpt.location).getMagnitude() - navWpt.radius)/1000 > 50000) {			
						distToGo = "DIST: " + String.format("%.2e", (Vector.subtract(spacecraft.location, navWpt.location).getMagnitude() - navWpt.radius)/1000) + " KM";
					} else {
						distToGo = "DIST: " + Integer.toString((int) ((Vector.subtract(spacecraft.location, navWpt.location).getMagnitude() - navWpt.radius)/1000)) + " KM";
					}
					fnt_moonhouse.draw(batch, distToGo, (float) (screen.x*0.02), (float) ((float) (screen.y) - (0.25*fontSize)));
				}
				
				fnt_moonhouse.draw(batch, speed, (float) (screen.x*0.02), (float) (screen.x*0.04));
				
				fnt_moonhouse.draw(batch, "DUR: " + timeYMDhms(missionDuration), (float) (screen.x*0.02), (float) ((screen.x*0.04) + (0.95*fontSize)));
				
				if(universe.drawOrbit) {
					if((spacecraft.relPos.getMagnitude() - universe.orbitalCenter.radius)/1000 > 50000) {			
						altitude = "ALT: " + String.format("%.2e", (spacecraft.relPos.getMagnitude() - universe.orbitalCenter.radius)/1000) + " KM";
					} else {
						altitude = "ALT: " + Integer.toString((int) ((spacecraft.relPos.getMagnitude() - universe.orbitalCenter.radius)/1000)) + " KM";
					}
					fnt_moonhouse.draw(batch, altitude, (float) (screen.x*0.02), (float) ((float) (screen.x*0.04) + (1.9*fontSize)));
					orbCenter = "ORBIT > " + universe.orbitalCenter.name;
					fnt_moonhouse.setColor(0,0.6f,1,1);
					fnt_moonhouse.draw(batch, orbCenter.toUpperCase(), (float) (screen.x*0.02), (float) ((float) (screen.x*0.04) + (2.85*fontSize)));
				}
				
				if(showInfoBox) {
					infoBoxPos = Vector.add(Vector.multiplyScalar(infoBoxOffset, infoScale/scaleValue),Vector.add(Vector.polar2Rect(Vector.subtract(infoBody.location, spacecraft.location).getMagnitude()*(screen.x/(200000000*scaleValue)), Vector.subtract(infoBody.location, spacecraft.location).getDirection() + spacecraft.heading), scr_offset));
					makeBodyInfoBox(infoBody);
					if(Gdx.input.isTouched()) {
						closeBtnPos.setXY(infoBoxPos.x + widest + (screen.x/100), infoBoxPos.y);
						if(Bounds.circle(closeBtnPos, touch, screen.x/32)) {
							showInfoBox = false;
							infoBoxinit = false;
							clickTimer = 0f;
						}
					}
				}
				
				// Messaging System
				
				fnt_moonhouse.setColor(1,0.9f,0,1);
				TextBounds tw_bounds = fnt_moonhouse.getBounds(msg);
				fnt_moonhouse.draw(batch, msg, (float) (screen.x/2 - (tw_bounds.width/2)), (float) (screen.y*0.8));
				
				batch.draw(dpad, screen.x-(4*(screen.x/16))-(screen.x/64), screen.x/64, screen.x/4, screen.x/4);
				
				timeWarp_slow.render(batch);
				timeWarp_fast.render(batch);
				zoomOut.render(batch);
				zoomIn.render(batch);
				
				batch.end();
				break;
/*####################################################################################################################*/
			case 3: // LAUNCH/LAND SCREEN
				
				
				
				break;
			
		}
		
	}
/*############################################# END OF RENDER METHOD #################################################*/
	
	// Convert time (from seconds to YMDhms format)
	public String timeYMDhms(float sec) {
		String result = "";
		
		long years = (long) Math.floor(sec/31557600);
		long months = (long) Math.floor((sec - (years*31557600))/2629800);
		long days = (long) Math.floor((sec - (years*31557600) - (months*2629800))/86400);
		long hours = (long) Math.floor((sec - (years*31557600) - (months*2629800) - (days*86400))/3600);
		long minutes = (long) Math.floor((sec - (years*31557600) - (months*2629800) - (days*86400) - (hours*3600))/60);
		long seconds = (long) Math.floor(sec - (years*31557600) - (months*2629800) - (days*86400) - (hours*3600) - (minutes*60));
		
		if(sec > 31557600) {
			result = String.format("%dY%dM%dD", years, months, days);
		} else if(sec > 2629800) {
			result = String.format("%dM%dD%dh", months, days, hours);
		} else if(sec > 86400) {
			result = String.format("%dD%dh%dm", days, hours, minutes);
		} else if(sec > 3600) {
			result = String.format("%dh%dm%ds", hours, minutes, seconds);
		} else if(sec > 60) {
			result = String.format("%dm%ds", minutes, seconds);
		} else {
			result = String.format("%ds", seconds);
		}
		
		return result;
	}
	
	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
		nFingers = 0;
		try {
			LocalData.saveData(spacecraft.getCurrentData(), "spacecraft.dat");
			System.out.println("Saving Spacecraft data...");
		} catch (IOException e) {
			System.out.println("[ERROR] IO Exception!");
			e.printStackTrace();
		}
		try {
			LocalData.saveData(universe.getCurrentData(), "universe.dat");
			System.out.println("Saving Universe data...");
		} catch (IOException e) {
			System.out.println("[ERROR] IO Exception!");
			e.printStackTrace();
		}
	}

	@Override
	public void resume() {
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// Pinch-to-zoom
		nFingers++;
		if(nFingers == 1) {
			finger1_ptr = pointer;
			finger1.set(screenX, screenY, 0);
		} else if(nFingers == 2) {
			finger2_ptr = pointer;
			finger2.set(screenX, screenY, 0);
			
			float dist = finger1.dst(finger2);
			lastDistance = dist;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// Pinch-to-zoom
		nFingers--;
		
		if(nFingers<0) {
			nFingers = 0;
		}
		
		lastDistance = 0;
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// Pinch-to-zoom
		if(pointer == finger1_ptr) {
			finger1.set(screenX, screenY, 0);
		}
		if(pointer == finger2_ptr) {
			finger2.set(screenX, screenY, 0);
		}
		
		float dist = finger1.dst(finger2);
		float factor = dist/lastDistance;
		
		if(lastDistance > dist) {
			scaleFactor -= factor;
		} else {
			// TODO Auto-generated method stub
			scaleFactor += factor;
		}
		
		lastDistance = dist;
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
	
	public void makeBodyInfoBox(CelestialBody body) {		
		if(!infoBoxinit) {
			dispOrder.clear();
			dispOrder.add(body.name.toUpperCase() + " [" + capitalizeFirstLetter(body.type) + "]");
			dispOrder.add("Period: " + timeYMDhms((float) body.orbPeriod));
			dispOrder.add("Speed: " + String.format("%.2f", (2*Math.PI*body.revRadius)/(1000*body.orbPeriod)) + " KM/S");
			if(body.radius > 1E7) {
				dispOrder.add("Radius: " + String.format("%.2e", body.radius/1000)+ " KM");
			} else {
				dispOrder.add("Radius: " + Integer.toString((int) (body.radius/1000)) + " KM");
			}
			if(body.mass > 1E4) {
				dispOrder.add("Mass: " + String.format("%.2e", body.mass)+ " KG");
			} else {
				dispOrder.add("Mass: " + Integer.toString((int) body.mass) + " KM");
			}
			if(navSet && (navWpt.name.equals(infoBody.name))) {
				widest = fnt_moonhouse.getBounds("UNSET WPT").width + 20;
			} else {
				widest = fnt_moonhouse.getBounds("SET NAV WPT").width + 20;
			}
			for(String line: dispOrder) {
				if(fnt_moonhouse.getBounds(line).width > widest) {
					widest = fnt_moonhouse.getBounds(line).width;
				}
			}
			infoBoxinit = true;
			infoBoxHeight = (float) ((fontSize*1.05) + ((dispOrder.size() - 1)*0.95*fontSize) + (screen.x/50) + (1.5*fontSize));
		}
		
		batch.draw(infobox, infoBoxPos.x, infoBoxPos.y - infoBoxHeight, widest + (screen.x/100), infoBoxHeight);
		batch.draw(closebtn, infoBoxPos.x + widest + (screen.x/100) - (screen.x/64), infoBoxPos.y - (screen.x/64), screen.x/32, screen.x/32);
		
		fnt_moonhouse.setColor(0.7f,0.7f,0.7f,1);
		
		for(int i = 1; i < dispOrder.size(); i++) {
			fnt_moonhouse.draw(batch, dispOrder.get(i), infoBoxPos.x + (screen.x/200) , (float) (infoBoxPos.y - (fontSize*((0.95*i) + 0.25))));
		}
		
		nav_target.origin.setXY(infoBoxPos.x + (screen.x/200), (float) (infoBoxPos.y - (fontSize*((0.95*dispOrder.size() + 2.25)))));
		nav_target.size.setXY(widest, (float) (fontSize*1.75));
		nav_target.render(batch);
		batch.draw(btnEdge, infoBoxPos.x + (screen.x/200), (float) (infoBoxPos.y - (fontSize*((0.95*dispOrder.size() + 2.25)))), 1 , (float) (fontSize*1.75));
		batch.draw(btnEdge, infoBoxPos.x + (screen.x/200) + widest - 1, (float) (infoBoxPos.y - (fontSize*((0.95*dispOrder.size() + 2.25)))), 1 , (float) (fontSize*1.75));
		fnt_moonhouse.setColor(1,1,1,1);
		fnt_moonhouse.draw(batch, dispOrder.get(0), infoBoxPos.x + (screen.x/200) , infoBoxPos.y);
		fnt_moonhouse.setColor(1,0.7f,0,1);
		if(navSet && (navWpt.name.equals(infoBody.name))) {
			fnt_moonhouse.draw(batch, "UNSET WPT", infoBoxPos.x + (widest/2) + (screen.x/200) - (fnt_moonhouse.getBounds("UNSET WPT").width/2), (float) (infoBoxPos.y - (fontSize*((0.95*dispOrder.size() + 0.75)))));
			if(nav_target.isClicked(touch) && clickTimer > 0.3) {;
				navSet = false;
				clickTimer = 0f;
			}
		} else {
			fnt_moonhouse.draw(batch, "SET NAV WPT", infoBoxPos.x + (widest/2) + (screen.x/200) - (fnt_moonhouse.getBounds("SET NAV WPT").width/2), (float) (infoBoxPos.y - (fontSize*((0.95*dispOrder.size() + 0.75)))));
			if(nav_target.isClicked(touch) && clickTimer > 0.3) {
				navWpt = infoBody;
				navSet = true;
				clickTimer = 0f;
			}
		}
		
	}
	
	public String capitalizeFirstLetter(String text) {
		return Character.toString(text.charAt(0)).toUpperCase() + text.substring(1,text.length());
	}
}
