// SpaceRace: Celestial Body Super Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class CelestialBody {

	public float mass, radius, radWithAtmos, revRadius, orbPeriod, theta, atmosThickness, density_sl;
	public Vector location, lastLoc;
	public Vector speed = new Vector();
	public String type; // star, planet, moon
	public String name;
	public Sprite sprite;
	public float e;
	public ArrayList<Planet> planets = new ArrayList<Planet>();
	public ArrayList<Moon> moons = new ArrayList<Moon>();
	public boolean hasAtmos;
	public boolean hasLandingPad;
	public boolean hasLaunchPad;
	public boolean hasRwy;
	
	public boolean int2Bool(int n) {
		if(n == 1) {
			return true;
		} else {
			return false;
		}
	}
	
}
