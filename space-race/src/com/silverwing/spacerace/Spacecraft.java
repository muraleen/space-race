// SpaceRace: Spacecraft Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Spacecraft {
	
	public Vector location, size, speed, absVel, relPos, dpadCenter;
	public float dpadRadius, mInertia, omega, mass, heading, fuelUnits, thrustMain, thrustMain_fc, thrustDir, thrustDir_fc, thrustRev, thrustRev_fc, dirCtlArm;
	public int burst; // 0:NO BURST, 1:FWD, 2:LEFT, 3:RIGHT, 4:REV
	public String textureDir;
	Sprite modules_attached, track_ptr, nav_ptr;
	Animation thr_fwd, thr_rev, thr_left, thr_right;
	TextureRegion[] thr_fwd_frames, thr_rev_frames, thr_left_frames, thr_right_frames;
	TextureRegion thr_current_frame;
	float stateTime, theta_click, dv;
	String refBody;
	boolean returnVal;
	PropertyList props;
	
	public Spacecraft(String name, Vector screen, Vector location, Vector speed, float heading, float fuelUnits, Vector dpadCenter, float dpadRadius) {
		
		props = new PropertyList();
		
		// Load lander data file
		textureDir = "spacecraft/"+name+"/";
		FileHandle fileHandle = Gdx.files.internal(textureDir+"module.dat");
		
		if(fileHandle.exists()) {			
			props = SRDataInterface.readPropsFile(fileHandle);
		}
		
		this.size = new Vector((props.getFloat("size-x")/1024)*screen.y, (props.getFloat("size-y")/1024)*screen.y);
		this.mass = props.getFloat("mass");
		this.mInertia = props.getFloat("moment-of-inertia");
		this.dirCtlArm = props.getFloat("dir-ctl-arm");
		this.thrustMain = props.getFloat("thrust-main");
		this.thrustMain_fc = props.getFloat("thrust-main-fc");
		this.thrustDir = props.getFloat("thrust-dir");
		this.thrustDir_fc = props.getFloat("thrust-dir-fc");
		this.thrustRev = props.getFloat("thrust-rev");
		this.thrustRev_fc = props.getFloat("thrust-rev-fc");
		
		this.modules_attached = GDXHelper.loadSprite(textureDir + props.getProp("texture-main"));
		Texture tmp_fwd_texture = GDXHelper.loadTexture(textureDir + props.getProp("texture-thr-fwd"));
		Texture tmp_rev_texture = GDXHelper.loadTexture(textureDir + props.getProp("texture-thr-rev"));
		Texture tmp_left_texture = GDXHelper.loadTexture(textureDir + props.getProp("texture-thr-left"));
		Texture tmp_right_texture = GDXHelper.loadTexture(textureDir + props.getProp("texture-thr-right"));
		thr_fwd_frames = TextureRegion.split(tmp_fwd_texture,tmp_fwd_texture.getWidth()/4,tmp_fwd_texture.getHeight())[0];
		thr_rev_frames = TextureRegion.split(tmp_rev_texture,tmp_rev_texture.getWidth()/2,tmp_rev_texture.getHeight())[0];
		thr_left_frames = TextureRegion.split(tmp_left_texture,tmp_left_texture.getWidth()/2,tmp_left_texture.getHeight())[0];
		thr_right_frames = TextureRegion.split(tmp_right_texture,tmp_right_texture.getWidth()/2,tmp_right_texture.getHeight())[0];
		thr_fwd = new Animation(0.03125f,thr_fwd_frames);
		thr_rev = new Animation(0.0625f,thr_rev_frames);
		thr_left = new Animation(0.0625f,thr_left_frames);
		thr_right = new Animation(0.0625f,thr_right_frames);
		
		this.burst = 0;
		this.speed = speed;
		this.absVel = speed;
		this.omega = 0; // Angular Velocity
		this.location = location;
		this.relPos = new Vector();
		this.heading = heading;
		this.fuelUnits = fuelUnits;
		
		stateTime = 0.0f;
		this.track_ptr = new Sprite(new Texture("overlay/track.png"));
		this.nav_ptr = new Sprite(new Texture("overlay/nav.png"));
		this.refBody = "";
		this.dpadCenter = dpadCenter;
		this.dpadRadius = dpadRadius;
	}
	
	public PropertyList getCurrentData() {
		PropertyList data = new PropertyList();
		// Vector location, Vector speed, float heading, float fuelUnits
		data.setProp("location-x", location.x);
		data.setProp("location-y", location.y);
		data.setProp("speed-x", absVel.x);
		data.setProp("speed-y", absVel.y);
		data.setProp("heading", heading);
		data.setProp("fuel-units", fuelUnits);
		return data;
	}
	
	public boolean control(Vector screen, Vector touch, float dt, float timeWarp, final Space_Race main) {
		
		returnVal = true;;
		
		if(Gdx.input.isTouched()) {
			if(Vector.subtract(touch, dpadCenter).getMagnitude() <= dpadRadius) {
				returnVal = false;
				theta_click = (float) (Vector.subtract(touch, dpadCenter).getDirection()*Vector.RAD2DEG);
				if((theta_click > -135) && (theta_click < -45)) {
					// Burst of thrust to slow down/reverse
					burst = 4;
					dv = (thrustRev*dt*timeWarp)/mass;
					fuelUnits -= thrustRev_fc*dt*timeWarp;
					speed.x -= dv*Math.sin(heading);
					speed.y -= dv*Math.cos(heading);
					absVel.x -= dv*Math.sin(heading);
					absVel.y -= dv*Math.cos(heading);
				} else if((theta_click < -135) || (theta_click > 135)) {
					// Burst of thrust to yaw left
					burst = 2;
					omega -= (dirCtlArm*thrustDir*dt)/mInertia; // CCW is positive
					fuelUnits -= thrustDir_fc*dt;
				} else if((theta_click > 45) && (theta_click < 135)) {
					// Burst of thrust to go forward
					if(timeWarp < 100) {
						burst = 1;
						dv = (thrustMain*dt*timeWarp)/mass;
						fuelUnits -= thrustMain_fc*dt*timeWarp;
						speed.x += dv*Math.sin(heading);
						speed.y += dv*Math.cos(heading);
						absVel.x += dv*Math.sin(heading);
						absVel.y += dv*Math.cos(heading);
					} else {
						main.msg = "Reduce timewarp to 50x!";
						main.timer.cancel();
						main.timer = new Timer();
						main.timer.schedule(new TimerTask() {
							@Override
							public void run() {
								main.msg = "";
							}	
						}, 2000);
					}
				} else {
					// Burst of thrust to yaw right
					burst = 3;
					omega += (dirCtlArm*thrustDir*dt)/mInertia; // CW is negative
					fuelUnits -= thrustDir_fc*dt;
				}
			}
		
			/* if(touch.x < screen.x*0.3) {
				// Burst of thrust to yaw left
				burst = 2;
				omega -= (dirCtlArm*thrustDir*dt)/mInertia; // CCW is positive
				fuelUnits -= thrustDir_fc*dt;
			} else if (touch.x > screen.x*0.7) {
				// Burst of thrust to yaw right
				burst = 3;
				omega += (dirCtlArm*thrustDir*dt)/mInertia; // CW is negative
				fuelUnits -= thrustDir_fc*dt;
			} else {
				if (touch.y > screen.y/3) {
					// Burst of thrust to go forward
					if(timeWarp < 100) {
						burst = 1;
						float dv = (thrustMain*dt*timeWarp)/mass;
						fuelUnits -= thrustMain_fc*dt*timeWarp;
						speed.x += dv*Math.sin(heading);
						speed.y += dv*Math.cos(heading);
					} else {
						main.msg = "Reduce timewarp to 50x!";
						main.timer.cancel();
						main.timer = new Timer();
						main.timer.schedule(new TimerTask() {
							@Override
							public void run() {
								main.msg = "";
							}	
						}, 2000);
					}
				} else {
					// Burst of thrust to slow down/reverse
					burst = 4;
					float dv = (thrustRev*dt*timeWarp)/mass;
					fuelUnits -= thrustRev_fc*dt*timeWarp;
					speed.x -= dv*Math.sin(heading);
					speed.y -= dv*Math.cos(heading);
				}
			} */
		} else {
			if(Gdx.input.isKeyPressed(Keys.UP)) {
				// Burst of thrust to go forward
				if(timeWarp<100) {
					burst = 1;
					float dv = (thrustMain*dt*timeWarp)/mass;
					fuelUnits -= thrustMain_fc*dt*timeWarp;
					speed.x += dv*Math.sin(heading);
					speed.y += dv*Math.cos(heading);
					absVel.x += dv*Math.sin(heading);
					absVel.y += dv*Math.cos(heading);
				} else {
					main.msg = "Reduce timewarp to 50x!";
					main.timer.cancel();
					main.timer = new Timer();
					main.timer.schedule(new TimerTask() {
						@Override
						public void run() {
							main.msg = "";
						}	
					}, 2000);
				}
			} else if(Gdx.input.isKeyPressed(Keys.DOWN)) {
				// Burst of thrust to slow down/reverse
				burst = 4;
				float dv = (thrustRev*dt*timeWarp)/mass;
				fuelUnits -= thrustRev_fc*dt*timeWarp;
				speed.x -= dv*Math.sin(heading);
				speed.y -= dv*Math.cos(heading);
				absVel.x -= dv*Math.sin(heading);
				absVel.y -= dv*Math.cos(heading);
			} else if(Gdx.input.isKeyPressed(Keys.LEFT)) {
				// Burst of thrust to yaw left
				burst = 2;
				omega -= (dirCtlArm*thrustDir*dt)/mInertia; // CCW is positive
			} else if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
				// Burst of thrust to yaw right
				burst = 3;
				omega += (dirCtlArm*thrustDir*dt)/mInertia; // CW is negative
			} else {
				burst = 0;
			}
		}
		
		if(Math.abs(omega) < 0.01) {
			omega = 0;
		}
		
		return returnVal;
		
	}
	
	public void render(SpriteBatch batch, Vector screen, float scale, Vector target, boolean navSet, boolean drawOrbit) {
		// Thrusters
		if(stateTime < 0.125f) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0.0f;
		}
		switch(burst) {
			case 1: // FWD
					thr_current_frame = thr_fwd.getKeyFrame(stateTime);
					break;
			case 2: // LEFT
				thr_current_frame = thr_left.getKeyFrame(stateTime);
					break;
			case 3: // RIGHT
				thr_current_frame = thr_right.getKeyFrame(stateTime);
					break;
			case 4: // REVERSE
				thr_current_frame = thr_rev.getKeyFrame(stateTime);
					break;
			// No default
		}
		if(burst != 0) {
			if(scale <= 0.0625) {
				batch.draw(thr_current_frame, (screen.x/2) - (size.x*8), (screen.y/2) - (size.y*8), size.x*16, size.y*16);
			} else if(scale <= 0.5) {
				batch.draw(thr_current_frame, (screen.x/2) - (size.x/(scale*2)), (screen.y/2) - (size.y/(scale*2)), size.x/scale, size.y/scale);
			} else {
				batch.draw(thr_current_frame, (screen.x/2) - (size.x), (screen.y/2) - (size.y), size.x*2, size.y*2);
			}
		}
		
		// Draw Spacecraft
		if(scale <= 0.0625) {
			batch.draw(modules_attached, (screen.x/2) - (size.x*8), (screen.y/2) - (size.y*8), size.x*16, size.y*16);
		} else if(scale <= 0.5) {
			batch.draw(modules_attached, (screen.x/2) - (size.x/(scale*2)), (screen.y/2) - (size.y/(scale*2)), size.x/scale, size.y/scale);
		} else {
			batch.draw(modules_attached, (screen.x/2) - (size.x), (screen.y/2) - (size.y), size.x*2, size.y*2);
		}
		
		// Draw Indicators
		if(speed.getMagnitude() >= 1) {
			if(drawOrbit) {
				batch.draw(track_ptr, (float) (screen.x*0.484375), screen.y/2, screen.x/64, 0, screen.x/32, (float) (screen.x*0.707), 1, 1, -90 + (speed.getDirection() + heading)*Vector.RAD2DEG);
			} else {
				batch.draw(track_ptr, (float) (screen.x*0.484375), screen.y/2, screen.x/64, 0, screen.x/32, (float) (screen.x*0.707), 1, 1, -90 + (absVel.getDirection() + heading)*Vector.RAD2DEG);
			}
		}
		if(navSet) {
			batch.draw(nav_ptr, (float) (screen.x*0.484375), screen.y/2, screen.x/64, 0, screen.x/32, (float) (screen.x*0.707), 1, 1, (float) (-90 + (heading + location.getCourseTo(target))*Vector.RAD2DEG));
		}
	}

}
