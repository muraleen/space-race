// SpaceRace: Data Interface File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class SRDataInterface {
	
	public static ArrayList<Star> getCelestialBodies() { // Return all celestial bodies
		
ArrayList<Star> returnData = new ArrayList<Star>();
		
		FileHandle stars_handle = Gdx.files.internal("data/stars.csv");
		
		if(stars_handle.exists()) {
			
			String[] starsData = stars_handle.readString().split("\r");
			
			for(String line: starsData) {
				String[] lineData = line.split(",");
				Star star_tmp = new Star(lineData[0], new Vector(Double.valueOf(lineData[3]).floatValue(),Double.valueOf(lineData[4]).floatValue()), Double.valueOf(lineData[1]).floatValue(), Double.valueOf(lineData[2]).floatValue(), new ArrayList<Planet>());
			
				// Get Planets' Data
				FileHandle planets_handle = Gdx.files.internal("data/"+lineData[0]+"/planets.csv");
				if(planets_handle.exists()) {
				
					String[] planetsData = planets_handle.readString().split("\r");
					
					for(String planets_line: planetsData) {
						String[] planets_lineData = planets_line.split(",");
						Planet planet_tmp = new Planet(planets_lineData[0], Double.valueOf(planets_lineData[1]).floatValue(), Double.valueOf(planets_lineData[2]).floatValue(), Double.valueOf(planets_lineData[3]).floatValue(), Double.valueOf(planets_lineData[4]).floatValue(), Double.valueOf(planets_lineData[5]).floatValue(), new ArrayList<Moon>(), Integer.valueOf(planets_lineData[6]), Integer.valueOf(planets_lineData[7]), Integer.valueOf(planets_lineData[8]), Integer.valueOf(planets_lineData[9]), Integer.valueOf(planets_lineData[10]), Double.valueOf(planets_lineData[11]).floatValue(), Double.valueOf(planets_lineData[12]).floatValue());
						
						// Get Moons' Data
						
						FileHandle moons_handle = Gdx.files.internal("data/"+lineData[0]+"/"+planets_lineData[0]+"_moons.csv");
						if(moons_handle.exists()) {
						
							String[] moonsData = moons_handle.readString().split("\r");
							
							for(String moons_line: moonsData) {
								String[] moons_lineData = moons_line.split(",");
								// Push Data to Planet
								planet_tmp.moons.add(new Moon(moons_lineData[0], Double.valueOf(moons_lineData[1]).floatValue(), Double.valueOf(moons_lineData[2]).floatValue(), Double.valueOf(moons_lineData[3]).floatValue(), Double.valueOf(moons_lineData[4]).floatValue(), Double.valueOf(moons_lineData[5]).floatValue()));
							}
						}
						// Push Data to star
						star_tmp.planets.add(planet_tmp);
					}
				}
				// Push Data to stars
				returnData.add(star_tmp);
				
			}
			
			return returnData;
			
		}
		
		// If it made through till here, return null
		
		return null;
	}
	
	public static ArrayList<Star> getCelestialBodies(String star) { // Return planets and moons associated with a giver star
		// This is the same as getting all bodies but this is easier on the device memory for gravitational calculations
		
		ArrayList<Star> returnData = new ArrayList<Star>();
		
		FileHandle stars_handle = Gdx.files.internal("data/stars.csv");
		
		if(stars_handle.exists()) {
			
			String[] starsData = stars_handle.readString().split("\r");
			
			for(String line: starsData) {
				String[] lineData = line.split(",");
				
				if(lineData[0] == star) {
				
					Star star_tmp = new Star(lineData[0], new Vector(Double.valueOf(lineData[3]).floatValue(),Double.valueOf(lineData[4]).floatValue()), Double.valueOf(lineData[1]).floatValue(), Double.valueOf(lineData[2]).floatValue(), new ArrayList<Planet>());
				
					// Get Planets' Data
					FileHandle planets_handle = Gdx.files.internal("data/"+lineData[0]+"/planets.csv");
					if(planets_handle.exists()) {
					
						String[] planetsData = planets_handle.readString().split("\r");
						
						for(String planets_line: planetsData) {
							String[] planets_lineData = planets_line.split(",");
							Planet planet_tmp = new Planet(planets_lineData[0], Double.valueOf(planets_lineData[1]).floatValue(), Double.valueOf(planets_lineData[2]).floatValue(), Double.valueOf(planets_lineData[3]).floatValue(), Double.valueOf(planets_lineData[4]).floatValue(), Double.valueOf(planets_lineData[5]).floatValue(), new ArrayList<Moon>(), Integer.valueOf(planets_lineData[6]), Integer.valueOf(planets_lineData[7]), Integer.valueOf(planets_lineData[8]), Integer.valueOf(planets_lineData[9]), Integer.valueOf(planets_lineData[10]), Double.valueOf(planets_lineData[11]).floatValue(), Double.valueOf(planets_lineData[12]).floatValue());
							
							// Get Moons' Data
							
							FileHandle moons_handle = Gdx.files.internal("data/"+lineData[0]+"/"+planets_lineData[0]+"_moons.csv");
							if(moons_handle.exists()) {
							
								String[] moonsData = moons_handle.readString().split("\r");
								
								for(String moons_line: moonsData) {
									String[] moons_lineData = moons_line.split(",");
									// Push Data to Planet
									planet_tmp.moons.add(new Moon(moons_lineData[0], Double.valueOf(moons_lineData[1]).floatValue(), Double.valueOf(moons_lineData[2]).floatValue(), Double.valueOf(moons_lineData[3]).floatValue(), Double.valueOf(moons_lineData[4]).floatValue(), Double.valueOf(moons_lineData[5]).floatValue()));
								}
							}
							// Push Data to Star
							star_tmp.planets.add(planet_tmp);
						}
					}
					// Push Data to stars
					returnData.add(star_tmp);
					
				}
				
			}
			
			return returnData;
			
		}
		
		// If it made through till here, return null
		
		return null;
	}
	
	public static String[] getStarsList() { // Return list of stars
		
		return null;
	}
	
	public static String[] getPlanetsList(String star) { // Return list of planets orbiting a star
		
		return null;
	}
	
	public static String[] getMoonsList(String planet) { // Return list of moons orbiting a planet
		
		return null;
	}
	
	public static PropertyList readPropsFile(FileHandle handle) {
		
		PropertyList props = new PropertyList();
		
		String[] landerData = handle.readString().split("\n");
		
		for(String dataLine: landerData) {
			String[] dataSet = dataLine.split("=");
			
			if(dataSet[1].substring(0,1).equals(">")) { // It's a number
				props.setProp(dataSet[0], Float.valueOf(dataSet[1].substring(1)));
			} else { // It's a string
				props.setProp(dataSet[0], dataSet[1]);
			}
			
		}
		
		return props;
	}

}
