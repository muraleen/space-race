// SpaceRace: Planet Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Planet extends CelestialBody {
	public boolean hasRings;
	public Sprite atmos;
	
	public Planet(String name, float mass, float revRadius, float orbPeriod, float radius, float theta, ArrayList<Moon> moons, int hasRings, int hasAtm, int hasLandPad, int hasRwy, int hasLaunchPad, float atmosThickness, float density_sl) {
		this.mass = mass;
		this.revRadius = revRadius*1000;
		this.orbPeriod = orbPeriod*31557600;
		this.radius = radius*1000;
		this.moons = moons;
		this.theta = theta*Vector.DEG2RAD;
		this.name = name;
		this.sprite = new Sprite(new Texture(Gdx.files.internal("bodies/"+name+".png")));
		this.hasRings = int2Bool(hasRings);
		this.hasAtmos = int2Bool(hasAtm);
		this.hasLandingPad = int2Bool(hasLandPad);
		this.hasLaunchPad = int2Bool(hasLaunchPad);
		this.hasRwy = int2Bool(hasRwy);
		this.atmosThickness = atmosThickness*1000;
		this.density_sl = density_sl;
		this.radWithAtmos = (this.radius + this.atmosThickness)*1.01f;
		this.type = "planet";
		
		if(this.hasAtmos) {
			if(Gdx.files.internal("bases/atmosphere/"+name+".png").exists()) {
				this.atmos = new Sprite(new Texture(Gdx.files.internal("bases/atmosphere/"+name+".png")));
			} else {
				this.atmos = new Sprite(new Texture(Gdx.files.internal("bases/atmosphere/generic.png")));
			}
		}
	}

}
