package com.silverwing.spacerace;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PropertyList implements Serializable {
	
	 ArrayList<Property> props;
	 Map<String, Integer> map;
	 
	 public PropertyList() { // Initialize property list
		 props = new ArrayList<Property>();
		 map = new HashMap<String, Integer>();
	 }
	 
	 public void setProp(String prop, float value) {
		 // Check if the property exists
		 if(map.containsKey(prop)) { // It exists! Set new value
			 props.get(map.get(prop)).setVal(value);
		 } else { // Doesn't exist, add property
			 props.add(new Property(prop, value));
			 map.put(prop, props.size() - 1);
		 }
	 }
	 
	 public void setProp(String prop, String value) {
		 // Check if the property exists
		 if(map.containsKey(prop)) { // It exists! Set new value
			 props.get(map.get(prop)).setVal(value);
		 } else { // Doesn't exist, add property
			 props.add(new Property(prop, value));
			 map.put(prop, props.size() - 1);
		 }
	 }
	 
	 public Object getProp(String prop) { // Get property value
		 if(map.containsKey(prop)) { // Check if it exists
			 if(props.get(map.get(prop)).type == 1) { // Return string
				 return props.get(map.get(prop)).getStringVal();
			 } else { // Return float
				 return props.get(map.get(prop)).getNumValue();
			 }
		 } else {
			 return null;
		 }
	 }
	 
	 public float getFloat(String prop) { // Get property value
		 if(map.containsKey(prop)) { // Check if it exists
			return props.get(map.get(prop)).getNumValue();
		 } else {
			 return 0;
		 }
	 }
	 
	 public void dump() {
		 for(Property prop: this.props) {
			 System.out.println(prop.name + " = [" + prop.getType() + "] " + prop.getStringVal());
		 }
	 }
	 
	 private static final long serialVersionUID = 1L;

}
