package com.silverwing.spacerace;

import java.io.Serializable;

public class Property implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public String name;
	public float numValue;
	public String textValue;
	public int type; // 0 - number, 1 - string
	
	public Property(String name, float value) {
		this.name = name;
		this.type = 0;
		this.numValue = value;
	}
	
	public Property(String name, String value) {
		this.name = name;
		this.type = 1;
		this.textValue = value;
	}
	
	public void setVal(float value) {
		this.type = 0;
		this.numValue = value;
	}
	
	public void setVal(String value) {
		this.type = 1;
		this.textValue = value;
	}
	
	public String getStringVal() {
		if(this.type == 1) {
			return textValue;
		} else {
			return String.valueOf(numValue);
		}
	}
	
	public float getNumValue() {
		if(this.type == 1) {
			return Float.valueOf(textValue);
		} else {
			return numValue;
		}
	}
	
	public String getType() {
		if(this.type == 1) {
			return "String";
		} else {
			return "Number";
		}
	}

}
