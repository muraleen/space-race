package com.silverwing.spacerace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class GDXHelper {

	public static Texture loadTexture(String path) {
		if(Gdx.files.internal(path).exists()) {
			return new Texture(Gdx.files.internal(path));
		} else {
			System.out.println("Cannot find texture: " + path);
			return new Texture(Gdx.files.internal("empty.png"));
		}
	}
	
	public static Sprite loadSprite(String path) {
		if(Gdx.files.internal(path).exists()) {
			return new Sprite(new Texture(Gdx.files.internal(path)));
		} else {
			System.out.println("Cannot find texture: " + path);
			return new Sprite(new Texture(Gdx.files.internal("empty.png")));
		}
	}
	
}
