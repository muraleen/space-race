package com.silverwing.spacerace;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.omg.CORBA_2_3.portable.OutputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class LocalData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	// Writes a property list object to file
	
	public static void saveData(PropertyList props, String fileString) throws IOException {
		FileHandle file = Gdx.files.local(fileString);
		// LocalData savedData = null;
		OutputStream out = null;
		try {
			file.writeBytes(serialize(props), false);
		} catch (Exception ex) {
			System.out.println(ex.toString());
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception ex) {}
			}
		}
	}
	
	// Reads a file and returns data as a property list
	
	public static PropertyList readData(String fileString) throws IOException, ClassNotFoundException {
		FileHandle file = Gdx.files.local(fileString);	
		return deserialize(file.readBytes());
	}
	
	private static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}
	
	public static PropertyList deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return (PropertyList) o.readObject();
	}
	
}
