// SpaceRace: Star Class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Star extends CelestialBody {
	
	public Star(String name, Vector location, float mass, float radius, ArrayList<Planet> planets) {
		this.location = location;
		this.mass = mass;
		this.radius = radius*1000;
		this.planets = planets;
		this.name = name;
		this.sprite = new Sprite(new Texture(Gdx.files.internal("bodies/"+name+".png")));
		this.type = "star";
	}
	
	public void process(float dt) {
		// Process Moon Positions
		for(Planet planet: this.planets) {
			planet.theta += (2*Math.PI*dt)/planet.orbPeriod;
			for(Moon moon: planet.moons) {
				moon.theta += (2*Math.PI*dt)/moon.orbPeriod;
			}
		}
	}
}
