package com.silverwing.spacerace;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Button_2State {
	
	public Vector origin, size;
	TextureRegion[] button;
	boolean state;
	
	public Button_2State(Vector origin, Vector size, String texturePath) {
		this.origin = origin;
		this.size = size;
		Texture buttonTexture = new Texture(Gdx.files.internal(texturePath));
		this.button = TextureRegion.split(buttonTexture, buttonTexture.getWidth()/2, buttonTexture.getHeight())[0];
	}
	
	public boolean isClicked(Vector touch) {
		if(Gdx.input.isTouched()) {
			if((touch.x > origin.x) && (touch.x < (origin.x + size.x)) && (touch.y > origin.y) && (touch.y < (origin.y + size.y))) {
				state = true;
				return true;
			} else {
				state = false;
				return false;
			}
		} else {
			state = false;
			return false;
		}
	}
	
	public void render(SpriteBatch batch) {
		if(state) {
			batch.draw(button[1], origin.x, origin.y, size.x, size.y);
		} else {
			batch.draw(button[0], origin.x, origin.y, size.x, size.y);
		}
	}

}
