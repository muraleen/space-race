// SpaceRace: Background Star class File
// Created by Narendran Muraleedharan
// Copyright (c) Silverwing 2014

package com.silverwing.spacerace;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BackgroundStar {
	
	public Sprite starSprite;
	public static float size[] = {2,3,6}, spdFactor = (float) 0.00000002;
	public float lastHdg = 0;
	public int layer;
	public Vector location;
	
	public BackgroundStar(Sprite sprite, int layer, Vector location) {
		starSprite = sprite;
		this.layer = layer;
		this.location = location;
	}
	
	public void update(Vector speed, float heading, float scale, float timeWarp) {
		float starHdg = speed.getDirection() + heading;
		Vector starSpd = new Vector();
		starSpd.setPolar((speed.getMagnitude()*spdFactor*size[layer]*timeWarp)/scale, starHdg);
		location.setPolar(location.getMagnitude(), location.getDirection()+((heading-lastHdg)));
		location = Vector.add(location, Vector.multiplyScalar(starSpd, -1));
		lastHdg = heading;
	}
	
	public void render(SpriteBatch batch, Vector screen) {
		batch.draw(starSprite, location.x + (screen.x/2), location.y + (screen.y/2), size[layer], size[layer]);
	}

}
